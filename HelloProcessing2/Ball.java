//
// Ball.java
//
// Darren Kessner
// Marlborough School
//


import processing.core.*;


public class Ball
{
    public Ball(PApplet papplet)
    {
        // save reference to PApplet

        this.papplet = papplet;

        // set other properties to random values

        c = papplet.color(papplet.random(255), papplet.random(255), papplet.random(255));
        x = papplet.random(radius, papplet.width-radius);
        y = papplet.random(radius, papplet.height-radius);
        vx = papplet.random(-3, 3);
        vy = papplet.random(-3, 3);
    }

    public void display()
    {
        // draw

        papplet.fill(c);
        papplet.ellipse(x, y, radius*2, radius*2);

        // update position

        x += vx;
        y += vy;

        // bounce off walls

        if (x<radius || x>papplet.width-radius)
            vx = -vx;

        if (y<radius || y>papplet.height-radius)
            vy = -vy;
    }

    private PApplet papplet;

    private int radius = 25;
    private int c;

    private float x;
    private float y;
    private float vx;
    private float vy;
}
