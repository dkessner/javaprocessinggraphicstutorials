//
// HelloProcessing2
//
// Darren Kessner
// Marlborough School
//


// This is a second example of using the Processing libraries to create a
// graphics application in Java.  This example illustrates how other classes
// can hold a reference to the PApplet class in order to use the Processing
// drawing functions.


import processing.core.*;
import java.util.*;


public class HelloProcessing2 extends PApplet
{
    public void settings()
    {
        size(400, 400); // must be in settings(), not setup()
    }

    public void setup()
    {
        balls.add(new Ball(this)); // start with single ball
    }

    public void draw()
    {
        background(0);

        fill(255);
        textSize(20);
        text("UP/DOWN: add/remove ball", 20, 50);

        for (Ball b : balls)
            b.display();
    }

    public void keyPressed()
    {
        if (keyCode == UP)
            balls.add(new Ball(this));
        else if (keyCode == DOWN && balls.size()>0)
            balls.remove(0);
    }

    private ArrayList<Ball> balls = new ArrayList<Ball>();

    public static void main(String[] args)
    {
        PApplet.main("HelloProcessing2");
    }
}


