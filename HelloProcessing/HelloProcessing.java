//
// HelloProcessing
//
// Darren Kessner
// Marlborough School
//


// This is a minimal example of using the Processing libraries to create a
// graphics application in Java.  To build/run this program, you must set the
// Java classpath to include Processing's core.jar (see the build/run scripts
// for the command line).


import processing.core.*;


public class HelloProcessing extends PApplet
{
    public void settings()
    {
        size(400, 400); // must be in settings(), not setup()
    }

    public void setup()
    {
    }

    public void draw()
    {
        background(0);
        fill(c);
        ellipse(x, y, radius*2, radius*2);

        x += vx;
        y += vy;

        if (x<radius || x>width-radius)
            vx = -vx;

        if (y<radius || y>height-radius)
            vy = -vy;
    }

    public void keyPressed()
    {
        if (key == ' ')
            c = color(random(255), random(255), random(255));
    }

    private int x = 200;
    private int y = 200;
    private int radius = 25;
    private int vx = 1;
    private int vy = 2;
    private int c = color(255);

    public static void main(String[] args)
    {
        PApplet.main("HelloProcessing");
    }
}


