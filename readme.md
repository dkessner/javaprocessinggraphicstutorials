Java Processing Graphics Tutorials
==================================

This project contains several examples of using the Processing graphics
libraries from Java.  These code examples will be of primary interest to
computer science educators who are using Java in their courses (e.g. AP
Computer Science A).

Each example has a `build` and `run` script showing how to build/run the
program from the command line.  The most important thing is to include
`core.jar` from the Processing libraries in the Java classpath.  The latest
`core.jar` is included in the `lib` directory for convenience.  To see the
examples in action, you can unzip the entire source tree and use the scripts to
build/run the example programs.

If you have any questions, feel free to email:
    Darren.Kessner@marlborough.org


